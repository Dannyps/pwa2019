app1.controller("Home", ['$http', 'common', function ($http, common) {
    var ctrl = this;
    ctrl.session = common.session;
    ctrl.message = {};
    ctrl.register = {};

    /*ctrl.creds = { email: 'aaa@aaa.com', password: 'a' };*/

    ctrl.doLogin = function () {
        $http.post('/login', ctrl.creds).then(
            function (rep) {
                common.session.login = rep.data.email;
                common.session.admin = rep.data.admin;
                common.rebuildMenu(); ctrl.message = { ok: 'Login successful' };
            },
            function (err) { ctrl.message = { error: 'Login failed' }; }
        );
    }

    ctrl.doLogout = function () {
        $http.delete('/login').then(
            function (rep) {
                ctrl.session.login = null;
                ctrl.session.admin = false;
                common.rebuildMenu(); ctrl.message = { ok: 'Logout successful' };
            }
        );
    }

    ctrl.doRegister = _ => {

        let elems = { "name": "name", "email": "e-mail", "password": "password", "bdate": "birth date" };
        for (let key in elems) {
            let value = elems[key];
            if (typeof ctrl.register[key] === 'undefined') {
                Swal.fire({
                    title: 'Error!',
                    text: 'The ' + value + ' must be filled!',
                    icon: 'error',
                    timer: 2500
                });
                return false;
            }
        };

        //console.log("Register process initiated.")
        if (ctrl.register.bdate > new Date(new Date() - (18 * 365 * 24 * 60 * 60 * 1000))) {
            Swal.fire(
                'Error!',
                'You must be 18 years old or older in order to create an account!',
                'error'
            )
            return false;
        }

        $http.post('/register', ctrl.register).then(
            function (rep) {
                Swal.fire(
                    'Success!',
                    'Your account request has been submited and will be reviewed by an operator shortly. We will email you with further instructions. Your case number id is <strong>' + rep.data.id + '</strong>.',
                    'success'
                )
                return true;
            },
            function (err) {
                Swal.fire(
                    'There was an error!',
                    err.data,
                    'error'
                )
                return true;
            }
        );
    }
}]);