app1.controller("Settings", ["$http", function ($http) {
    var ctrl = this;
    ctrl.newpass = { password: '' };
    ctrl.retype = '';
    ctrl.message = {};
    ctrl.edited = { name: '', recipient: '' };
    ctrl.selectedIndex = -1;

    ctrl.doChangePassword = function () {
        $http.put('/login', ctrl.newpass).then(
            function (rep) { ctrl.message = { ok: 'Password changed' }; },
            function (err) { ctrl.message = { error: 'Password not changed' }; }
        );
    };

    ctrl.doNew = function () {
        $http.post('/predefined', ctrl.edited).then(
            function (rep) { ctrl.doRefresh(); },
            function (err) { }
        );
    };

    ctrl.doClick = function (index) {
        ctrl.selectedIndex = index;
        ctrl.edited.name = ctrl.defs[index].name;
        ctrl.edited.recipient = ctrl.defs[index].recipient;
    };

    ctrl.doUpdate = function (index) {
        $http.put('/predefined?_id=' + ctrl.defs[ctrl.selectedIndex]._id, ctrl.edited).then(
            function (rep) { ctrl.doRefresh(); },
            function (err) { }
        )
    };

    ctrl.doRefresh = function () {
        $http.get('/predefined').then(
            function (rep) { ctrl.defs = rep.data; },
            function (err) { ctrl.def = []; }
        );
    };

    ctrl.doRefresh();
}]);