app1.controller("Transfer", ["$http", "$uibModal", "common", function ($http, $uibModal, common) {
    var ctrl = this;
    ctrl.account = {};
    ctrl.transfer = { recipient: '', amount: 1, description: '' };

    var zeroData = function () {
        ctrl.transfer = { recipient: '', amount: 0, description: '' };
        ctrl.message = {};
    }

    ctrl.doTransfer = function () {
        $http.post('/account', ctrl.transfer).then(
            function (rep) { ctrl.account = rep.data; refreshRecipients(); zeroData(); ctrl.message = { ok: 'Transfer successful' }; },
            function (err) { ctrl.message = { error: 'Transfer failed' }; }
        );
    };

    ctrl.transferInvalid = function () {
        return isNaN(ctrl.transfer.amount) || ctrl.transfer.amount <= 0 || ctrl.account.balance - ctrl.transfer.amount < ctrl.account.limit;
    }

    $http.get('/account').then(
        function (rep) { ctrl.account = rep.data; },
        function (err) { ctrl.account = {}; }
    );

    var refreshRecipients = function () {
        $http.get('/recipients').then(
            function (rep) { ctrl.recipients = rep.data; },
            function (err) { ctrl.recipients = []; }
        );
    }

    refreshRecipients();

    ctrl.modalOpen = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title-top',
            ariaDescribedBy: 'modal-body-top',
            templateUrl: '/html/modalTransfer.html',
            controller: 'ModalTransfer',
            controllerAs: 'ctrl',
            resolve: {
                recipients: function () {
                    return ctrl.recipients;
                }
            }
        });
        modalInstance.result.then(
            function (data) {
                ctrl.transfer = data;
                ctrl.doTransfer()
            }, function (error) {
                console.info('Modal dismissed at: ' + new Date().toISOString() + " via " + error)
            });
    };

    ctrl.confirm = function () {
        common.confirm("Confirm the operation", function (answer) {
            if(answer)
                ctrl.doTransfer();
        });
    };

}]);