app1.controller("History", ["$http", function ($http) {
    var ctrl = this;
    ctrl.last5 = true;
    ctrl.filter = '';
    ctrl.history = [];

    ctrl.refreshHistory = function () {
        var to = ctrl.last5 ? 5 : 999999;
        $http.get('/history?from=1&to=' + to + '&filter=' + ctrl.filter).then(
            function (rep) { ctrl.history = rep.data; },
            function (err) { ctrl.message = 'Error retrieving history'; }
        );
    };

    ctrl.refreshHistory();
}]);