app1.controller("Admin", ['$http', 'common', function ($http, common) {
    var ctrl = this;
    ctrl.admin = [];
    ctrl.admin.requestsL = 0;
    ctrl.admin.status = [];
    ctrl.admin.status.options = ["any", "new", "accepted", "rejected"]
    ctrl.admin.status.selected = "new"; // default value

    ctrl.admin.reload = function () {
        $http.get('/register?filter=' + ctrl.admin.status.selected).then(
            function (rep) {
                rep.data.forEach(function (val, index) {
                    let d = new Date(val.bDate);
                    rep.data[index].bDateFormated = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
                }); // use arr as this
                ctrl.admin.allRequests = rep.data;
                ctrl.admin.requests = rep.data;
                ctrl.admin.requestsL = ctrl.admin.requests.length;

                // Initialize tooltip component
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })

                // Initialize popover component
                $(function () {
                    $('[data-toggle="popover"]').popover()
                })

            },
            function (err) {
                Swal.fire(
                    'Error retrieving history!',
                    err.data,
                    'error'
                );
            }
        );
    }

    ctrl.admin.reject = function (index) {
        let obj = ctrl.admin.requests[index];
        obj.status = "rejected";
        //obj.statusDate = new Date(); // server side fo this please
        ctrl.admin.put(obj);
        ctrl.admin.reload();
    }

    ctrl.admin.accept = function (index) {
        let obj = ctrl.admin.requests[index];
        obj.status = "accepted";
        //obj.statusDate = new Date(); // server side fo this please
        ctrl.admin.put(obj);
        ctrl.admin.reload();
    }

    ctrl.admin.applyFilter = function () {
        ctrl.admin.requests = [];
        ctrl.admin.allRequests.forEach(e => {
            if (e.name.toLowerCase().indexOf(ctrl.admin.nameFilter.toLowerCase()) !== -1)
                ctrl.admin.requests.push(e);
        });
    }

    ctrl.admin.getClass = function (status) {
        if (status == 'accepted') return "alert-success";
        else if (status == 'rejected') return "alert-danger";
        else return "alert-info";
    }

    ctrl.admin.put = function (obj) {
        $http.put('/register', obj).then(
            function (rep) {
                Swal.fire(
                    'Success!',
                    'The status has been changed to <strong>' + rep.data.status + '</strong>.',
                    'success'
                )
            },
            function (err) {
                Swal.fire(
                    'Error updating status!',
                    err.data.err,
                    'error'
                );
            }
        );
    }


    ctrl.admin.reload();
    // first load
}]);