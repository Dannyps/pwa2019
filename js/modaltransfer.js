app1.controller("ModalTransfer", ["$uibModalInstance", "recipients", function ($uibModalInstance, recipients) {
    console.log("ModalTransfer controller started");
    var ctrl = this;
    ctrl.recipients = recipients;
    ctrl.transfer = { recipient: '', amount: 1, description: '' };
    ctrl.submit = function () {
        $uibModalInstance.close(ctrl.transfer);
    }
}]);