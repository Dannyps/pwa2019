var fs = require('fs');
var path = require('path');
var mime = require('mime');

var lib = module.exports = {

    sendJSON: function (rep, code, desc, obj) {
        rep.writeHead(code, desc, { 'Content-type': 'application/json' });
        rep.end(JSON.stringify(obj));
    },

    serveStaticContent: function (response, absPath) {
        var n = absPath.indexOf('?');
        var fileName = absPath.substring(0, n != -1 ? n : absPath.length);
        fs.exists(fileName, function (exists) {
            if (exists) {
                fs.readFile(fileName, function (err, data) {
                    if (err) {
                        lib.sendErrorOnStaticContent(response, 406);
                    } else {
                        lib.sendFile(response, fileName, data);
                    }
                });
            } else {
                if (fileName.endsWith('.map')) {
                    console.log('skipping request for ' + absPath);
                    lib.sendJSON(response, 200, 'OK', {});
                } else {
                    lib.sendErrorOnStaticContent(response, 404);
                }
            }
        });
    },

    sendErrorOnStaticContent: function (response, code) {
        console.log('sending error ' + code + ' page');
        response.writeHead(code, { 'Content-Type': 'text/plain; charset=utf-8' });
        switch (code) {
            case 404:
                response.write('Error 404: file not found.');
                break;
            case 403:
                response.write('Error 403: access denied.');
                break;
            case 406:
                response.write('Error 406: not acceptable');
                break;
            default:
                response.write('Error ' + code);
        }
        response.end();
    },

    sendFile: function (response, filePath, fileContents) {
        var mimeType = mime.getType(path.basename(filePath));
        response.writeHead(200, { 'Content-Type': mimeType });
        console.log('sending file ' + filePath + ' ' + mimeType);
        response.end(fileContents);
    },

    getPayload: function (req, rep, callback) {
        req.setEncoding('utf8');
        var payload = '';
        req.on('data', function (data) { payload += data; }).on('end', function () {
            try {
                var obj = JSON.parse(payload);
                callback(req, rep, null, obj);
            } catch (ex) {
                callback(req, rep, { msg: ex.message }, null);
            }
        });
    }

};